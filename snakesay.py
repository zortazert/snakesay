#!/usr/bin/env python3
import sys
import textwrap
import random
import json
import os
import platform
import logging

if os.getenv('DEBUG'):
    logging.basicConfig(level=logging.DEBUG)

snake = """
         \\        ___
          \\   \\__| o \\
              /   \\  |
                   | |     o
                 __| |__  //
                |_______|//
                \\_______//
"""
cow = """
        \\   ^__^
         \\  (oo)\\_______
            (__)\\       )\\/\\
                ||----w |
                ||     ||
"""
tux = """
       \\
        \\
         _--_
        (o_ o)
        |<_) |
      _//    \\\\
     (_|     |_)
     |  \___/  |
     \\__/---\\__/ 
"""

share_dir = '/usr/local/share' if platform.system() == 'Darwin' else '/usr/share'

if os.path.exists(f"{share_dir}/fortunes.txt") == True:
    with open(f"{share_dir}/fortunes.txt", "r") as f:
        fortunes = f.read()
        fortunes = fortunes.splitlines()
else:
    logging.debug('Not using fortunes.txt from share dir')

    with open("fortunes.txt", "r") as f:
        fortunes = f.read()
        fortunes = fortunes.splitlines()


def error():
    print("ERROR: GIVE ME TEXT!")
    quit()


if len(sys.argv) < 2:
    error()

args = sys.argv[1:]
text = ''
thing = snake

if "-fortune" in args:
    text = fortunes[random.randint(0, len(fortunes))]
    thing = snake

if "-r" in args:
    rand = random.randint(1, 3)
    if rand == 1:
        thing = snake
    if rand == 2:
        thing = cow
    if rand == 3:
        thing = tux

    index = args.index('-r')

    if not '-fortune' in args:
        text = ' '.join(args[index+1:])
elif "-s" in args:
    thing = snake

    index = args.index('-s')

    if not '-fortune' in args:
        text = ' '.join(args[index+1:])
elif "-c" in args:
    thing = cow

    index = args.index('-c')

    if not '-fortune' in args:
        text = ' '.join(args[index+1:])
elif "-t" in args:
    thing = tux

    index = args.index('-t')

    if not '-fortune' in args:
        text = ' '.join(args[index+1:])
elif "-h" in args or "--help" in args:
    print(f"""This is a less feature rewrite of cowsay in python.

python {sys.argv[0]} QUERY - make a snake say something

python {sys.argv[0]} -fortune - make a snake say a random fortune. Requires requests installed.

python {sys.argv[0]} -r QUERY - randomly selected a tux, cow or snake will say something

python {sys.argv[0]} -t QUERY - a tux will say something

python {sys.argv[0]} -s QUERY - a snake will say something

python {sys.argv[0]} -c QUERY - a cow will say something""")
    quit()
else:
    if not text:
        text = ' '.join(args)

if text == "":
    error()
dashes = len(text) + 2
if dashes > 30:
    dashes = 30
    print(" " + "-"*dashes)
    text = textwrap.fill(text, dashes-2).split("\n")
    for enter in text:
        stuff = "| " + enter
        length = dashes - len(stuff)
        print(stuff + " "*length+" |")
    print(" "+"-"*dashes)
else:
    print(" "+"-"*dashes)
    print("| "+text+" |")
    print(" "+"-"*dashes)

print(thing)
